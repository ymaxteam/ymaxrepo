<?php 
include "header.php";
?>
<!-- Banner -->
<div class="rates_banner">
  <div class="container text-center">
	    <div class="call_form rate_pg_form">
	        <form action="test.php">
	            <div class="form_block">
	                <input type="search" autocomplete="off" id="rate_search" name="search" placeholder="Which country you are calling to?">
	                <ul id="live"></ul>
	            </div>
	            <div class="get_call_lnk">
	                <a href="#" data-toggle="modal" data-target="#rate_pop">Get Rate</a>
	            </div>
	        </form>
	    </div>	
	</div>
</div>
<!--Rates-->

<div class="rate_list_part">
  <div class="container">
    <div class="head_rate">
      <h3>RATES</h3>
    </div>
    <p style="text-align:center;">Make cheap International and domestic calls at low rates.</p>
  </div>

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 " >
    
    <div class="rate_warp">
      <table class="table table-striped">
      <?php
      
      require("ymaxadmin/config/config.inc.php"); 
require("ymaxadmin/config/Database.class.php");
require("ymaxadmin/config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$rateQry 		= 	"SELECT ID,country,code,rate FROM ".TABLE_PRICELIST." ORDER BY country ASC";
$rateResult		=	mysql_query($rateQry);
$num 	= 	0;
while($rateRow		=	mysql_fetch_array($rateResult))
{
	$rate[$num]['ID']		=	$rateRow['ID'];
	$rate[$num]['country']	=	$rateRow['country'];
	$rate[$num]['code']		=	$rateRow['code'];
	$rate[$num]['rate']		=	$rateRow['rate'];
	
	$num++;
	
}
$i=0;
$asciiCap=65;
$asciiSmall=97;
?>
 
<?php
while($i<count($rate) )
{	
 	
	
	
	 
      ?>
      <thead style="border-bottom:1px solid #FFF; border-top:1px solid #FFF;">
      <tr>
        <th colspan="3" style="font-size:32px; text-align:center; width:100%;" id="<?php echo chr($asciiCap)?>"><?php echo chr($asciiCap)?></th>
      </tr>
       <tr>
        <th colspan="3" class="alphabetic_anchor_lnk" style="text-align:center; border:none;"><a href="#A"> A </a> <a href="#B">B</a> <a href="#C">C</a> <a href="#D">D </a><a href="#E">E </a><a href="#F">F</a> <a href="#G">G</a> <a href="#H">H</a> <a href="#I">I</a> <a href="#J">J</a> <a href="#K">K</a> <a href="#L">L</a> <a href="#M">M</a> <a href="#N">N</a> <a href="#O">O</a> <a href="#P">P</a> <a href="#Q">Q</a> <a href="#R">R</a> <a href="#S">S</a> <a href="#T">T</a> <a href="#U">U</a> <a href="#V">V</a> <a href="#W">W</a> <a href="#X">X</a> <a href="#Y">Y</a> <a href="#Z">Z</a></th>
      </tr>
    </thead>
    
     <tbody>
      <thead style="border-bottom:1px solid #FFF; border-top:1px solid #FFF;">
      <tr>
        <th>Destination</th>
        <th class="country_code">Country Code</th>        
        <th>USD/min	</th>
      </tr>
      </thead>
     <?php
     $status=1;
      while($status==1 && $i<count($rate))
		{
			$countryName	=	substr($rate[$i]['country'],0,1);
			$cc=(string)chr($asciiCap);
			$ss = (string)chr($asciiSmall);
	       	if($countryName==$cc || $countryName==$ss )
			{
		     ?>
		      <tr style="background:#064475;">
		      	<td><?php echo $rate[$i]['country'] ?></td>
		      	<td class="country_code"><?php echo $rate[$i]['code'] ?></td>		        
		        <td><?php echo '$'.$rate[$i]['rate'] ?></td>
		      </tr>
		      <?php
		      	$i++;
			}
			else
			{
				$status=0;
			}
		}	
      ?>
      </tbody>
      <?php 
	$asciiCap++;
	$asciiSmall++;
} ?>

      </table>
      
      </div>
      
    </div>
  </div>
</div>
</br>
</div>

<?php include "footer.php";?>