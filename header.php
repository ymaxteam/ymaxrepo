<!DOCTYPE HTML>
<html lang="">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="images/fav_icon.png">
<title>YMAX</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/script.js"></script>
</head>

<body>
<!-- Header -->
<div class="header"> <span class="nav_trigger"><i class="fa fa-bars"></i></span>
  <div class="container">
    <div class="logo"> <a href="index.php"> <img src="images/logo.png" alt="" /> </a> </div>
    <div class="nav_wrap">
      <ul class="navigation">
        <li><a href="index.php">home</a></li>
       <!-- <li><a href="#">about</a></li>-->
        <li><a href="#" id="download">download</a></li>
        <li><a href="rates.php">rates</a></li>
        <li><a href="#" data-toggle="modal" data-target="#myModal">Login</a></li>
        <!--<li><a href="#">buy credit</a></li>-->
        <li><a href="#" data-toggle="modal" data-target="#support_pop">support</a></li>
      </ul>
    </div>
  </div>
</div>
<!-- /Header --> 

