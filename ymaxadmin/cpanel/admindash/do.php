<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid	=	$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_FILES['file']['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{	
				
				$success=0;
				
				ini_set('max_execution_time', 0); 			
				
				//$examId		=	$App->convert($_REQUEST['examId']);
				$file = $_FILES['file']['name'];/*echo $file;die;*/
				
					$chk_exit=explode('.',$file);					
					if(strtolower(end($chk_exit))=="csv")
					{
						$filename=$_FILES['file']['tmp_name'];
						//echo $filename;
						$handle = fopen($filename, "r");
						fgetcsv($handle,0, ",");
						
						$delete		=	mysql_query("TRUNCATE TABLE ".TABLE_PRICELIST);
						while(($filesop = fgetcsv($handle, 0, ",")) !== FALSE)
						{
							
							 $country	= 	$filesop[0];	//country
							 $code 		= 	$filesop[1];  	//code
							 $rate		= 	$filesop[2];	//rate
							
													
							$qry = "INSERT INTO ".TABLE_PRICELIST."(country,code,rate) VALUES ('$country','$code','$rate')";
							$success = mysql_query($qry);
						}
						
						fclose($handle);
						
						if($success)
						{						
							$_SESSION['msg']="<font color='white'>Data imported successfully</font>";
							header("location:index.php");		
						}
						else
						{
							$_SESSION['msg']="<font color='red'>Failed</font>";
							header("location:index.php");	
						}		
					}
					else
					{
						$_SESSION['msg']="<font color='red'>Invalid file format</font>";
					}
					header("location:index.php");
				}	
		break;
		
	// EDIT SECTION
	case 'edit':	
				if(!$_REQUEST['fid'])
				{
					$_SESSION['msg']="<font color='red'>Error, Invalid Details!</font>";					
					header("location:index.php");	
				}
				else
				{
					$fid = $_REQUEST['fid'];
					$success=0;
					
					$data['country']		=	$App->convert($_REQUEST['country']);
					$data['code']			=	$App->convert($_REQUEST['code']);
					$data['rate']			=	$App->convert($_REQUEST['rate']);
						
					$success = $db->query_update(TABLE_PRICELIST, $data ,"ID='{$fid}'");	
										
					$db->close();
					
					if($success)
					{						
						$_SESSION['msg']="<font color='white'>Data updated successfully</font>";
						header("location:index.php");		
					}
					else
					{
						$_SESSION['msg']="<font color='red'>Updation Failed</font>";
						header("location:index.php");	
					}
				}
	break;	
	// DELETE SECTION
	case 'delete':	
			if(!$_REQUEST['id'])
			{
				$_SESSION['msg']="<font color='red'>Error, Invalid Details!</font>";					
				header("location:index.php");	
			}
			else
			{
				$id = $_REQUEST['id'];
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_PRICELIST."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="<font color='red'>You can't delete.</font>";				            
				}																
				$db->close(); 					                
        
        
				if($success)
				{
				     $_SESSION['msg']="<font color='white'>Deleted Successfully</font>";					
					
				}
				else
				{
				$_SESSION['msg']="<font color='red'>You can't delete.</font>";	
								
				}
				header("location:index.php");
			}	
}
?>