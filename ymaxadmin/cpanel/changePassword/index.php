<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();


 if(isset($_SESSION['msg'])){echo $_SESSION['msg']; }	
 $_SESSION['msg']='';
?>
<script>
function valid()
{

flag=false;

jPassword=document.getElementById('newPassword').value;
jConfirmPassword=document.getElementById('conPassword').value;		
		if(jConfirmPassword=="" || jConfirmPassword!=jPassword)
		{																			///for password
		document.getElementById('pwddiv').innerHTML="Password And Confirm Password are Not Equal";
		flag=true;
		}

if(flag==true)
	{
		return false;
	}

}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
 
      <!-- Modal1 -->
      <div>
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="../admindash/index.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">CHANGE PASSWORD</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">			
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label for="oldUserName">Existing User name:<span class="star">*</span></label>
                      <input type="text" class="form-control2" name="oldUserName" id="oldUserName" required >
                    </div>
					
					<!--<div class="form-group">
                      <label for="newUserName">New User name:</label>
                      <input type="text" class="form-control2" name="newUserName" id="newUserName" required  >
                    </div>-->
					
				
					<div class="form-group">
						<label for="oldPassword">Existing Password:<span class="star">*</span></label>
						<input type="password" name="oldPassword" id="oldPassword" class="form-control2" required >
					</div>
					
					<div class="form-group">
						<label for="newPassword" >New Password:<span class="star">*</span></label></br >
						<input type="password" name="newPassword" id="newPassword" class="form-control2" required>
					</div>
					
					<div class="form-group">
						<label for="conPassword">Confirm Password:<span class="star">*</span></label>	
						<input type="password" name="conPassword" id="conPassword" class="form-control2" required onfocus="clearbox('pwddiv')">
						 <div id="pwddiv" class="valid" style="color:#FF9900;"></div> 
					</div>					
				</div>
                   </div>  				             
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>