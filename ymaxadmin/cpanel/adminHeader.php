<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>YMAX</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery_pagination.js"></script>


<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newstyle.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css"/>


<script>
	/*$(document).ready(function() {
        $('.leftmenu ul > li').mouseenter(function() {
            //$(this).find('ul').show();
			var windWidth = $(window).width();
			var cur = $(this);
			var curSub = cur.find('ul');
			if(windWidth >= 992){
				$('.leftmenu ul > li > ul').not(cur.find('ul')).removeClass('openMenu');
				cur.find('ul').addClass('openMenu');
			}
        });
    });*/
</script>

<!--<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container">  
  <a  href="index.php" >
  <img src="../../img/logo.png" alt="" /></a> 
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/index.php">Change Password</a></li>          
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">