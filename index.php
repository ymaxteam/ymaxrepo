<?php include "header.php";?>

    <!-- Banner -->
    <div class="banner">
        <div class="banner_caption">
            <div class="container">
                <div class="cap_block">
                    <h2>Ymax</h2>
                    <p>Comfort and convenience anywhere in the world, and enjoy the amazing quality VoIP service with low rates to anyone from any of your phones and computer with YMAX. And, save up to maximum% on your international calling costs beginning with your very next phone call by using YMAX.</p>
                    <!--<div class="raed_more">
                        <a href="#">Read More...</a>
                    </div>-->
                    <div class="store_lnk">
                        <a href="https://itunes.apple.com/in/app/ymax/id729245209?mt=8" class="apple"><i class="fa fa-apple"></i></a>
                        <a href="https://play.google.com/store/apps/details?id=com.vox.ymax" class="android"><i class="fa fa-android"></i></a>
                        <a href="https://www.microsoft.com/en-us/store/apps/ymax/9nblggh0g0hz" class="windows"><i class="fa fa-windows"></i></a>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    
    <div class="container free_wrap">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="free_img">
                    <img src="images/free_pic.png" alt="" />
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="free_text">
                    <h3>free voice &amp; video calls</h3>
                    <p>There’s a reason this is called a smart phone system. We can make an international phone call is easy and cheap with YMAX. Its provides real rates [no hidden charges] that are cheaper than many international calling cards and convenient features like Quick Dial and Online Call Details etc. Get YMAX and Comfort and convenience anywhere in the world.</p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="desk_wrap">
        <div class="container">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="desk_text">
                    <h3>Ymax,<br />For Desktop</h3>
                    <p>You will get full freedom with YMAX, it will help you to connect with everyone in various ways. Install YMAX App in your computer and use all benefits. Get YMAX and Comfort and convenience anywhere in the world.</p>
                    <div class="desk_lnk">
                        <a href="http://www.ymaxvoice.com/Ymax.msi" download>download</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="desk_img">
                    <img src="images/desk_img.png" alt="" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="container call_wrap">
        <div class="call_head">
            <div class="call_head_inner">
                <h3>Make cheap International and domestic calls at low rates.</h3>
                <div class="call_head_lnk">
                    <a href="rates.php">Learn more...</a>
                </div>                
            </div>
        </div>
        <div class="call_form">
            <form action="test.php">
                <div class="form_block">
                    <input type="search" autocomplete="off" id="rate_search" name="search" placeholder="Which country you are calling to?">
                    <!--<button type="submit" name="search"><i class="fa fa-search"></i></button>-->
                    <ul id="live"></ul>
                </div>
                <div class="get_call_lnk">
                    <a href="#" data-toggle="modal" data-target="#rate_pop">Get Rate</a>
                </div>
            </form>
        </div>
        <div class="call_img">
            <img src="images/call_img.png" alt="" />
        </div>
    </div>
    
    
   <?php include "footer.php";?>