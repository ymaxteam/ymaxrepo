<!-- Footer -->
<div class="footer">
  <div class="footer_inner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="footer_text">
            <h2>Ymax</h2>
            <p>Comfort and convenience anywhere in the world</p>
            <ul>
              <li>Save up to maximum% on your international calling costs</li>
              <li>It's provides real rates [no hidden charges] that are cheaper than many international calling cards</li>
              <li>Convenient features like Quick Dial and Online Call Details etc.</li>
              <li>You will get full freedom with YMAX</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="copy_wrap">
            <div class="copy_rt"> Copyright &copy; 2016 Ymax Blue App. </div>
            <div class="power"> Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi.png" alt="" /></a> </div>
            <div class="bd_clear"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Login</h4>
      </div>
      <div class="modal-body">
        <form class="login">
        	<div class="row">
        		<div class="col-lg-6 col-md-6 col-sm-6">
        			<div class="form_block">
        				<label>User Name</label>
        				<input class="log_input" type="text" name="username" placeholder="Please enter your user name"/> 
        			</div>        			
        		</div>
        		<div class="col-lg-6 col-md-6 col-sm-6">
        			<div class="form_block">
        				<label>Password</label>
        				<input class="log_input" type="password" name="password" placeholder="Please enter your password"/>
        			</div>   
        		</div>
        	</div>
        	<p class="msg_invalid">Invalid user name or password. Please try again.</p>
        	<div class="submit text-right">
        		<button type="submit" name="login" class="btn btn-primary btn_login">Login</button>
        	</div>
        </form>
      </div>
    </div>

  </div>
</div>
<!-- Modal -->
<div id="support_pop" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Contact Us</h4>
      </div>
      <div class="modal-body">
      	<p class="support_text">You can reach to us at any time. It's our pleasure to serve you. We wolud love to hear from you.</p>
      	<div class="support_det">
      		<i class="fa fa-envelope"></i>
      		<a href="mailto:support@ymax.com">support@ymax.com</a>
      	</div>  
      	<div class="support_det">
      		<i class="fa fa-phone"></i>
      		<a href="tel:1234567890">1234 567890</a>
      	</div> 
      	<div class="contact_form">
      		<form method="post" action="mail.php">
      			<div class="cont_block">
      				<input type="text" class="form-control" name="name" required="" placeholder="Please enter your name here"/>
      			</div>
      			<div class="cont_block">
      				<input type="email" class="form-control" name="email" required="" placeholder="Please enter your email ID here"/>
      			</div>
      			<div class="cont_block">
      				<input type="text" class="form-control" name="mobile" placeholder="Please enter your contact number here"/>
      			</div>
      			<div class="cont_block">
      				<input type="text" class="form-control" name="chat" required="" placeholder="Skype/Gtalk etc"/>
      			</div>
      			<div class="cont_block">
      				<textarea name="message" class="form-control" placeholder="Please enter your message here"></textarea>
      			</div>
      			<div class="cont_block">
      				<input type="submit" name="submit" value="Send"/>
      			</div>
      		</form>
      	</div>
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="rate_pop" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Rate</h4>
      </div>
      <div class="modal-body">
      	<div class="table-responsive">
      		<table class="table table-bordered">
      			<thead>
      				<tr>
      					<th>Code</th>
      					<th>Country</th>
      					<th>Rate</th>
      				</tr>
      			</thead>
      			<tbody>
      				<tr>
      					<td class="code"></td>
      					<td class="country"></td>
      					<td class="rate"></td>
      				</tr>      				
      			</tbody>
      		</table>
      	</div> 
      </div>
    </div>

  </div>
</div>




<div class="over_lay"></div>

</body>
</html>