/*jslint browser: true*/
/*global $, jQuery, alert*/
$(function () {
    'use strict';
    $(window).scroll(function () {
        var windTop = $(window).scrollTop();
        if (windTop >= 300) {
            $('.header').addClass('shrink');
            $('.header').dequeue();
        } else {
            $('.header').removeClass('shrink');
            $('.header').dequeue();
        }
    });
    $('.nav_trigger').click(function () {
        $('.navigation').slideToggle();
    });
    $(document).on('click', '.alphabetic_anchor_lnk a', function(e){
    	e.preventDefault();
    	var anchorTarget = $($(this).attr('href')),
    		anchorPosition = parseInt(anchorTarget.offset().top);
    	//console.log(anchorPosition);
    	$('html, body').animate({
    		scrollTop : parseInt(anchorPosition) - 100
    	}, 2000);
    });
    
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
        {
            return 'iOS';

        }
        else if( userAgent.match( /Android/i ) )
        {

            return 'Android';
        }
        else
        {
                return 'unknown'; 
        }
    }
    //var osDetect = getMobileOperatingSystem(),
    var osDetect = navigator.platform,
    	iosLnk = "https://itunes.apple.com/in/app/ymax/id729245209?mt=8",
        androidLnk = "https://play.google.com/store/apps/details?id=com.vox.ymax",
        wpLnk = "https://www.microsoft.com/en-us/store/apps/ymax/9nblggh0g0hz",
        bbLnk = "https://appworld.blackberry.com/webstore/content/50254887/?lang=en&countrycode=IN",
        symbLnk = "http://ymaxvoice.com/YmaxBlue.php",
        wPcLnk = "http://www.ymaxvoice.com/Ymax.msi";
        //alert(osDetect);
    
    /*if(osDetect == 'Android'){
        $('.download').attr('href', androidLnk);
    } else if(osDetect == 'iOS'){
        $('.download').attr('href', iosLnk);
    }*/
    switch (osDetect) {
		case 'Linux armv7l': // Android
			$('#download').attr('href', androidLnk);
			break;
		case 'Linux aarch64': // Android
			$('#download').attr('href', androidLnk);
			break;
		case 'Linux i686': // Android
			$('#download').attr('href', androidLnk);
			break;
		case 'Linux x86_64': // Android
			$('#download').attr('href', androidLnk);
			break;
		case 'Linux MSM8960_V3.2.1.1_N_R069_Rev:18': // Android
			$('#download').attr('href', androidLnk);
			break;
		case 'iPhone': // iOS
			$('#download').attr('href', iosLnk);
			break;
		case 'iPad': // iOS
			$('#download').attr('href', iosLnk);
			break;
		case 'iPod': // iOS
			$('#download').attr('href', iosLnk);
			break;
		case 'Win32': // Windows
			$('#download').attr('href', wPcLnk);
			break;
		case 'Win64': // Windows
			$('#download').attr('href', wPcLnk);
			break;
		case 'Symbian': // Nokia
			$('#download').attr('href', symbLnk);
			break;
		case 'ARM': // WP
			$('#download').attr('href', wpLnk);
			break;
		default:
			$('#download').attr('href', 'javascript:void(0)');
			break;
	}
    
    
    //console.log(navigator.platform);
    $('.btn_login').click(function(e){
    	e.preventDefault();
    	var regValid = 1;
    	$('.log_input').each(function(){
    		var curInput = $(this);
			if(curInput.val()){
				$('.msg_invalid').removeClass('active');
				//regValid = 1;
			} else {
				$('.msg_invalid').addClass('active');
				regValid = 0;
			}
    	});
    	if(regValid != 0){
			$('.over_lay').show();
			setTimeout(function(){
				$('.msg_invalid').addClass('active');
				$('.over_lay').hide();
			}, 3000);
		}
    });
    $('#myModal button.close').click(function(){
    	$('.msg_invalid').removeClass('active');
    	$('.log_input').val('');
    });
    $('.modal-backdrop').click(function(){
    	$('.msg_invalid').removeClass('active');
    	$('.log_input').val('');
    });
    
    // Live search
    $('#rate_search').keyup(function(){
    	var searchTerm = $(this).val();
    	//console.log(searchTerm);
    	$.ajax({
    		type: 'POST',
    		url: 'rateloader.php',
    		data: 'search='+searchTerm,
    		success: function(data){
				//console.log(data);
				$('#live').html(data);
			},
			error: function(){
				//console.log('error');
				$('#live').html('<li>Something went wrong. Please Try again.</li>');
			}
    	});
    });
    $(document).on('click', '#live li.has_data', function(){
    	var curLiveLi = $(this),
    		curCode = curLiveLi.data('code'),
    		curCountry = curLiveLi.data('country'),
    		curRate = curLiveLi.data('rate'),
    		tableTarget = $('#rate_pop table');
    	//console.log('Code: '+ curCode + '\n' + 'Country: ' + curCountry + '\n' + 'Rate: ' + curRate);
    	$('#rate_search').val(curCode + ' - ' + curCountry);
    	tableTarget.find('td.code').text(curCode);
    	tableTarget.find('td.country').text(curCountry);
    	tableTarget.find('td.rate').text(curRate);
    	$('#live').html('');    	
    });
    
});













